// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

USTRUCT()
struct FSpawnPosition
{
	GENERATED_USTRUCT_BODY()

	FVector Location;
	float Rotation;
	float Scale;
};

USTRUCT(BlueprintType)
struct FSpawnParameters
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, Category = Spawning)
	int32 MinSpawn;
	
	UPROPERTY(BlueprintReadWrite, Category = Spawning)
	int32 MaxSpawn;
	
	UPROPERTY(BlueprintReadWrite, Category = Spawning)
	float Radius;
	
	UPROPERTY(BlueprintReadWrite, Category = Spawning)
	float MinScale;
	
	UPROPERTY(BlueprintReadWrite, Category = Spawning)
	float MaxScale;

	UPROPERTY(BlueprintReadWrite, Category = Spawning)
	FVector MinExtent;

	UPROPERTY(BlueprintReadWrite, Category = Spawning)
	FVector MaxExtent;

	FSpawnParameters()
	{
		MinSpawn = 1;
		MaxSpawn = 1;
		MinScale = 1;
		MaxScale = 1;
		Radius = 500;
		MinExtent = FVector(800, -2500, 100);
		MaxExtent = FVector(5000, 2500, 100);
	}
};

class UActorPool;

UCLASS()
class S05_TESTINGGROUNDS_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	UFUNCTION(BlueprintCallable, Category = "Spawning")
		void PlaceActors(TSubclassOf<AActor> ToSpawn, FSpawnParameters SpawnParameters);

	UFUNCTION(BlueprintCallable, Category = "Spawning")
		void PlaceAIPawns(TSubclassOf<APawn> ToSpawn, FSpawnParameters SpawnParameters);

	UFUNCTION(BlueprintCallable, Category = "Pool")
		void SetPool(UActorPool* Pool);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditDefaultsOnly, Category = Navigation)
	FVector NavigationBoundsOffset;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	const int MAX_ATTEMPTS = 100;
	UActorPool* Pool = nullptr;
	AActor* NavMeshBoundsVolume = nullptr;

	template<class T>
	void RandomlyPlaceActors(TSubclassOf<T>, FSpawnParameters);
	void PlaceActor(TSubclassOf<AActor>&, const FSpawnPosition&);
	void PlaceActor(TSubclassOf<APawn>&, const FSpawnPosition &SpawnPosition);
	bool FindEmptyLocation(FVector&, float, FSpawnParameters);
	bool CanSpawnAtLocation(FVector, float);
	void PositionNavMeshBoundsVolume();
};