// Fill out your copyright notice in the Description page of Project Settings.

#include "ChooseNextWaypoint.h"
#include "PatrolRoute.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Classes/AIController.h"

EBTNodeResult::Type UChooseNextWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	// Get the blackboard component
	UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
	
	// Get patrol route
	auto Pawn = OwnerComp.GetAIOwner()->GetPawn();
	if (!ensure(Pawn)) { return EBTNodeResult::Failed; }
	UPatrolRoute* PatrolRoute = Pawn->FindComponentByClass<UPatrolRoute>();

	// Get patrol points
	if (!ensure(PatrolRoute)) { return EBTNodeResult::Failed; }
	TArray<AActor*> PatrolPoints = PatrolRoute->GetPatrolPoints();

	// Set next waypoint
	if (!ensure(BlackboardComp)) { return EBTNodeResult::Failed; }
	if (PatrolPoints.Num() == 0)
	{ 
		UE_LOG(LogTemp, Warning, TEXT("A guard is missing patrol points"));
		return EBTNodeResult::Failed; 
	}
	int Index = BlackboardComp->GetValueAsInt(IndexKey.SelectedKeyName);
	BlackboardComp->SetValueAsObject(WaypointKey.SelectedKeyName, PatrolPoints[Index]);

	// Cycle index
	int NewIndex = ++Index % PatrolPoints.Num();
	BlackboardComp->SetValueAsInt(IndexKey.SelectedKeyName, NewIndex);

	return EBTNodeResult::Succeeded;
}


